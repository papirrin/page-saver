import page_saver
from bs4 import BeautifulSoup, Tag

page_url = 'vademecum'
page_output = page_url + '.html'
base_url = 'http://santgar.com/'
base_url_pattern = 'http[s]?://santgar.com/'
output_path = '../USB_root/LaboratorioSantgar/'
unwanted_tags = ["footer"]
unwanted_ids = ["zone-menu-wrapper", "region-branding-top","region-branding-bottom", "skip-link"]
unwanted_strs = [('Producto en la tienda virtual', 1, True), \
                 ('url: "http://santgar.com/hit_counter_piwik_ajax"', 0, True)]

to_add =   [('^http://santgar.com/vademecum-details/*', Tag.insert_after, 'region-branding',
             '''<div class="grid-8 region region-branding-top" id="region-branding-top">
  <div class="region-inner region-branding-top-inner">
    <div class="block block-menu block-menu-user-menu block-menu-menu-user-menu odd block-without-title" id="block-menu-menu-user-menu">
  <div class="block-inner clearfix">
                
    <div class="content clearfix">
      <ul class="menu"><li class="first leaf"><a id="offline-home-link" href="http://santgar.com/vademecum.html">Ver todos los medicamentos</a></li>
</ul>    </div>
  </div>
</div>  </div>
</div>''')]

page_saver.download_page(page_url, base_url, base_url_pattern, output_path,\
                         unwanted_tags, unwanted_ids, unwanted_strs, False, True, page_output, to_add)

#print(sorted(page_saver.all_downloaded))
