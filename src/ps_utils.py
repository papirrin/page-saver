import os
import urllib.request


def retrieve_page(page_url):
    page = urllib.request.urlopen(page_url)
    return page.read().decode()

def url_as_filename(u):
    u = u.replace('?', '_q_')
    u = u.replace('%', '_p_')
    u = u.replace('=', '_e_')
    return u

def create_subfolders(sub):
    sub_dir = os.path.dirname(sub)
    if not os.path.exists(sub_dir):
        os.makedirs(sub_dir)

def unique_and_sorted(l):
    l = list(set(l))
    l.sort()
    return l
