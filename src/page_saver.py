import re
import os
from bs4 import BeautifulSoup, Doctype
from time import sleep

from ps_utils import *


def create_string_filter(to_search, ignore_case):
    to_search = to_search.lower() if ignore_case else to_search
    
    def str_filter(tag):

        #print(tag)
        t = tag.string
        if not t:
            return None
        if ignore_case:
            t = t.lower()
        return to_search in t
    return str_filter

def remove_string_matches(soup, with_str, up_levels, ignore_case):
    str_filter = create_string_filter(with_str, ignore_case)
    matches = soup.find_all(str_filter)
    for m in matches:
        for i in range(up_levels):
            m = m.parent
        extracted = m.extract()
        #print(extracted)
    return soup
    
def remove_unwanted(page, tags, ids, unwanted_strs, base_to, base_to_escaped):
    soup = BeautifulSoup(page, "html5lib")
    for d in tags:
        for tag in soup.find_all(d):
            tag.extract()
    #By IDs
    for d in ids:
        for tag in soup.find_all(id=d):
            tag.extract()

    for d in unwanted_strs:
        text, levels, ignore_case = d
        soup = remove_string_matches(soup, text, levels, ignore_case)

    # TODO: Refactor to fit this modifications in some design
    for s in soup.find_all('script'):
        s_type = s.get('type')
        if s_type and 'text/javascript' in s_type:
            s_src = s.get('src')
            if s_src and s_src.startswith('http://maps.googleapis.com'):
                s.extract()
    for c_i in soup.find_all('div', 'cloud-zoom-container'):
        for a in c_i.find_all('a'):
            del a['href']
            #a.href.extract()
        del c_i['class']
        del c_i['id']
    for c_t in soup.find_all('div', 'cloud-zoom-gallery-thumbs'):
        c_t.extract()

    hide_loading = '''
<style>
div.cloud-zoom-loading{visibility:hidden}
#offline-home-link{font-size:20px;padding:20px}
</style>'''
    soup.head.insert_after(hide_loading);
    return soup

def add_html(url, soup, to_add_list):
    for to_add in to_add_list:
        filter_url, f, ref_id, html = to_add
        if re.match(filter_url, url):
            f(soup.find(id = ref_id), html)
    return soup

all_downloaded = set()
        
def download_url_content(page, base_from, base_to, output_path, create_files, ignore_list):
    re_urlchar = '(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))'
    pattern_from = base_from + re_urlchar + '+'
    rest = False
    total_downloaded = 0
    step_downloaded = 0
    
    urls = re.findall(pattern_from, page)
    if create_files:
        # Create files
        for u in urls:
            if u in ignore_list or u in all_downloaded:
                continue
            sub = url_as_filename(re.sub(base_from, output_path, u))
            create_subfolders(sub)
            with open(sub, 'wb') as f_o:
                try:
                    #print(u)
                    f_o.write(urllib.request.urlopen(u).read())
                except urllib.error.HTTPError as e:
                    print("HTTP reason({0}): {1} code: {2}".format(e.reason, e.code, u))
                all_downloaded.add(u)
            # Throttle the download rate to prevent 403 errors
            if rest:
                sleep(5)
                rest = False
                step_downloaded = 0
            step_downloaded += 1
            total_downloaded += 1
            if step_downloaded > 20:
                rest = True

    # Replace url in files
    for u in urls:
        page = page.replace(u, url_as_filename(u))
    page = re.sub(base_from, base_to, page)
    return page

def expand_nesting_path(nesting_path):
    return '/'.join(nesting_path)

def replace_abs_paths(page, nesting_path, page_output):
    new_soup = BeautifulSoup(page, "html5lib")
    links = new_soup.body.find_all('a')
    for l in links:
        href = l.get('href')
        if href == None:
            continue
        if href == "/":
            # Hacked
            #l['href'] = expand_nesting_path(nesting_path) + '/' + page_output
            l['href'] = './' + "../" * (len(nesting_path)-1) + 'vademecum.html'
        elif href.startswith("/"):
            l['href'] = expand_nesting_path(nesting_path)+ href
    return new_soup



def download_page( page_url, base_url, base_url_pattern, output_path,\
                  unwanted_tags, unwanted_ids, unwanted_strs, download_files, download_links, \
                  page_output, to_add=[], nesting_path = ['.']):

    print('start recursion: ' + page_url)
    output_base = './' + "../" * (len(nesting_path)-1)
    page = retrieve_page(base_url + page_url)
    soup = remove_unwanted(page, unwanted_tags, unwanted_ids, unwanted_strs, output_base, '.\/' + "..\/" * (len(nesting_path)-1))
    soup = add_html(base_url + page_url, soup, to_add)

    #print('with class:', [l for l in soup.body.find_all('a') if l.get('class') != None])
    #print('with class.classes:', [l.get('class') for l in soup.body.find_all('a') if l.get('class') != None])
    #print('with class cloud-zoom:', [l for l in soup.body.find_all('a') if l.get('class') == ])
    # TODO: Ignore list is site dependent??
    ignore_list = unique_and_sorted({l.get('href') for l in soup.body.find_all('a') \
                                     if l.get('href')!= None and \
                                     l.get('class') != None and 'cloud-zoom' in l.get('class') and\
                                     l.get('id') != None and 'cloud-zoom' in l.get('id')})
    
    
    reduced_page = soup.encode(formatter=None).decode()
    new_page = download_url_content(reduced_page, base_url_pattern, \
                                    output_base, output_path, \
                                    download_files, ignore_list)

    new_soup = replace_abs_paths(new_page, nesting_path, page_output)

    items = [item for item in new_soup.contents if isinstance(item, Doctype)]

    # Insert MOTW for Internet Explorer
    items[0].insert_after('<!-- saved from url=(00'+ str(len(base_url))+')'+base_url+' -->\n')
    new_page = new_soup.encode(formatter=None).decode()

    full_path = output_path + expand_nesting_path(nesting_path) + '/'+page_output
    create_subfolders(full_path)
    with open(full_path, 'w') as f_o:
        f_o.write(new_page)

    if download_links:
        links_to_explore = set()
        links = new_soup.body.find_all('a')
        for l in links:
            href = l.get('href')
            if href == None:
                continue
            # only add links in same page hierarchy
            if href.startswith(expand_nesting_path(nesting_path)) and \
               href != expand_nesting_path(nesting_path) + '/' +page_output:
                links_to_explore.add(href)
        links_to_explore = unique_and_sorted(links_to_explore)
        for l in links_to_explore:
            new_page_url = l[len(expand_nesting_path(nesting_path))+1:]
            splitted_url = new_page_url.split('/')
            new_nesting_path = nesting_path + splitted_url[:-1]
            new_page_output = splitted_url[-1]
            download_page(new_page_url, base_url, base_url_pattern, output_path,\
                          unwanted_tags, unwanted_ids, unwanted_strs, download_files, download_links,\
                          new_page_output, to_add, new_nesting_path)
                          
    print('finish recursion: ' + page_url)


